; GCC "intrinsics" file entry point.
;
; This is invoked to build support files for registering intrinsic
; functions within gcc. this code has a fair bit of target-specific
; code in it. it's not a general-purpose module yet.
;
; Copyright (C) 2000, 2009 Red Hat, Inc.
; This file is part of CGEN.

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "guile")
	   (load-from-path "read")
	   (load-from-path "intrinsics"))

(define intrinsics-isas '())

(define intrinsics-arguments
  (list
   (list "-K" "isa" "keep isa <isa> in intrinsics" #f
	 (lambda (args)
	   (for-each
	    (lambda (arg) (set! intrinsics-isas (cons (string->symbol arg) intrinsics-isas)))
	    (string-cut args #\,))))
   (list "-M" "file" "generate insns.md in <file>" #f
	 (lambda (arg) (file-write arg insns.md)))
   (list "-N" "file" "generate intrinsics.h in <file>" #f
	 (lambda (arg) (file-write arg intrinsics.h)))
   (list "-P" "file" "generate intrinsic-protos.h in <file>" #f
	 (lambda (arg) (file-write arg intrinsic-protos.h)))
   (list "-T" "file" "generate intrinsic-testsuite.c in <file>" #f
	 (lambda (arg) (file-write arg intrinsic-testsuite.c)))))

; Main routine, parses options and calls generators.

(define (cgen-intrinsics argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "intrinsics"
	  #:arg-spec intrinsics-arguments
	  #:analyze intrinsics-analyze!)
    )
)

(cgen-intrinsics (program-arguments))
