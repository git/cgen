; CPU description file generator for CGEN cpu documentation
; This is invoked to build: $arch.html.
; Copyright (C) 2003, 2009 Doug Evans
; This file is part of CGEN.

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "read")
	   (load-from-path "desc")
	   (load-from-path "desc-cpu")
	   (load-from-path "html"))

(define doc-arguments
  (list
   (list "-H" "file" "generate $arch.html in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen.html)))
   (list "-I" "file" "generate $arch-insn.html in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-insn.html)))
   (list "-N" "file" "specify name of insn.html file"
	 #f
	 (lambda (arg) (set! *insn-html-file-name* arg)))
   )
)

; Main routine, parses options and calls generators.

(define (cgen-doc argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "doc"
	  #:arg-spec doc-arguments
	  #:init doc-init!
	  #:finish doc-finish!
	  #:analyze doc-analyze!)
    )
)

(cgen-doc (program-arguments))
