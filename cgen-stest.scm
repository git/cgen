; CPU description file generator for the simulator testsuite.
; Copyright (C) 2000, 2009 Red Hat, Inc.
; This file is part of CGEN.

; This is invoked to build several .s files and a script to run to
; generate the .d files and .exp file.
; This is invoked to build: tmp-build.sh cpu-cpu.exp

(use-modules (ice-9 slib))

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "read")
	   (load-from-path "desc")
	   (load-from-path "desc-cpu")
	   (load-from-path "opcodes")
	   (load-from-path "opc-asmdis")
	   (load-from-path "opc-ibld")
	   (load-from-path "opc-itab")
	   (load-from-path "opc-opinst")
	   (load-from-path "sim-test"))

(define stest-arguments
  (list
   (list "-B" "file" "generate build.sh"
	 #f
	 (lambda (arg) (file-write arg cgen-build.sh)))
   (list "-E" "file" "generate the testsuite .exp"
	 #f
	 (lambda (arg) (file-write arg cgen-allinsn.exp)))
   )
)

; Main routine, parses options and calls generators.

(define (cgen-stest argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "sim-test"
	  #:arg-spec stest-arguments
	  #:init sim-test-init!
	  #:finish sim-test-finish!
	  #:analyze sim-test-analyze!)
    )
)

(cgen-stest (program-arguments))

;; FIXME: cgen-all will generate the opcodes files, not what we want
