; CPU description file generator for the GNU Binutils.
; This is invoked to build: $arch-desc.[ch], $arch-opinst.c,
; $arch-opc.h, $arch-opc.c, $arch-asm.in, $arch-dis.in, and $arch-ibld.[ch].
; Copyright (C) 2000, 2009 Red Hat, Inc.
; This file is part of CGEN.

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "read")
	   (load-from-path "desc")
	   (load-from-path "desc-cpu")
	   (load-from-path "opcodes")
	   (load-from-path "opc-asmdis")
	   (load-from-path "opc-ibld")
	   (load-from-path "opc-itab")
	   (load-from-path "opc-opinst"))

(define opc-arguments
  (list
   (list "-OPC" "file" "specify path to .opc file"
	 (lambda (arg) (set-opc-file-path! arg))
	 #f)
   (list "-H" "file" "generate $arch-desc.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-desc.h)))
   (list "-C" "file" "generate $arch-desc.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-desc.c)))
   (list "-O" "file" "generate $arch-opc.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-opc.h)))
   (list "-P" "file" "generate $arch-opc.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-opc.c)))
   (list "-Q" "file" "generate $arch-opinst.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-opinst.c)))
   (list "-B" "file" "generate $arch-ibld.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-ibld.h)))
   (list "-L" "file" "generate $arch-ibld.in in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-ibld.in)))
   (list "-A" "file" "generate $arch-asm.in in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-asm.in)))
   (list "-D" "file" "generate $arch-dis.in in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-dis.in)))
   )
)

; (-R "file" "generate $cpu-reloc.h") ; FIXME: wip (rename to -abi.h?)
; (-S "file" "generate cpu-$cpu.c") ; FIXME: wip (bfd's cpu-$cpu.c)
; ((-R) (file-write *arg* cgen-reloc.c))
; ((-S) (file-write *arg* cgen-bfdcpu.c))

; Main routine, parses options and calls generators.

(define (cgen-opc argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "opcodes"
	  #:arg-spec opc-arguments
	  #:init opcodes-init!
	  #:finish opcodes-finish!
	  #:analyze opcodes-analyze!)
    )
)

(cgen-opc (program-arguments))
