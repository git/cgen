; Simulator generator entry point.
; This is invoked to build: arch.h, cpu-<cpu>.h, memops.h, semops.h, decode.h,
; decode.c, defs.h, extract.c, semantics.c, ops.c, model.c, mainloop.in.
;
; memops.h, semops.h, ops.c, mainloop.in are either deprecated or wip.
;
; Copyright (C) 2000, 2009 Red Hat, Inc.
; This file is part of CGEN.

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "read")
	   (load-from-path "utils-sim")
	   (load-from-path "sim")
	   (load-from-path "sim-arch")
	   (load-from-path "sim-cpu")
	   (load-from-path "sim-model")
	   (load-from-path "sim-decode"))

(define sim-arguments
  (list
   (list "-A" "file" "generate arch.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-arch.h)))
   (list "-B" "file" "generate arch.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-arch.c)))
   (list "-C" "file" "generate cpu-<cpu>.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-cpu.h)))
   (list "-U" "file" "generate cpu-<cpu>.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-cpu.c)))
   (list "-N" "file" "generate cpu-all.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-cpuall.h)))
   (list "-F" "file" "generate memops.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-mem-ops.h)))
   (list "-G" "file" "generate defs.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-defs.h)))
   (list "-P" "file" "generate semops.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-sem-ops.h)))
   (list "-T" "file" "generate decode.h in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-decode.h)))
   (list "-D" "file" "generate decode.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-decode.c)))
   (list "-E" "file" "generate extract.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-extract.c)))
   (list "-R" "file" "generate read.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-read.c)))
   (list "-W" "file" "generate write.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-write.c)))
   (list "-S" "file" "generate semantics.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-semantics.c)))
   (list "-X" "file" "generate sem-switch.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-sem-switch.c)))
   (list "-O" "file" "generate ops.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-ops.c)))
   (list "-M" "file" "generate model.c in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-model.c)))
   (list "-L" "file" "generate mainloop.in in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-mainloop.in)))
   )
)

; Main routine, parses options and calls generators.

(define (cgen-sim argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "sim"
	  #:arg-spec sim-arguments
	  #:init sim-init!
	  #:finish sim-finish!
	  #:analyze sim-analyze!)
    )
)

(cgen-sim (program-arguments))
