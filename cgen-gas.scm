; CPU description file generator for the GAS testsuite.
; Copyright (C) 2000, 2001, 2009 Red Hat, Inc.
; This file is part of CGEN.

; This is invoked to build several .s files and a "build script",
; which generates the .d files and .exp DejaGNU test case.

(eval-when (expand load eval)
	   ;; Kept global so it's available to the other .scm files.
	   (define srcdir (dirname (current-filename)))
	   (add-to-load-path srcdir)

	   (load-from-path "read")
	   (load-from-path "desc")
	   (load-from-path "desc-cpu")
	   (load-from-path "opcodes")
	   (load-from-path "opc-asmdis")
	   (load-from-path "opc-ibld")
	   (load-from-path "opc-itab")
	   (load-from-path "opc-opinst")
	   (load-from-path "gas-test"))

(define gas-arguments
  (list
   (list "-B" "file" "generate build script in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-build.sh)))
   (list "-E" "file" "generate allinsn.exp in <file>"
	 #f
	 (lambda (arg) (file-write arg cgen-allinsn.exp)))
   )
)

; Main routine, parses options and calls generators.

(define (cgen-gas argv)
  (let ()

    (display-argv argv)

    (cgen #:argv argv
	  #:app-name "gas-test"
	  #:arg-spec gas-arguments
	  #:init gas-test-init!
	  #:finish gas-test-finish!
	  #:analyze gas-test-analyze!)
    )
)

(cgen-gas (program-arguments))

;; FIXME: cgen-all will generate the opcodes files, not what we want
