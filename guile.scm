; Guile-specific functions.
; Copyright (C) 2000, 2004, 2009 Red Hat, Inc.
; This file is part of CGEN.
; See file COPYING.CGEN for details.

(define (eval1 expr)
  (eval expr (current-module)))

(define %stat stat)

(debug-enable 'backtrace)

(define (debug-write . objs)
  (map (lambda (o)
	 ((if (string? o) display write) o (current-error-port)))
       objs)
  (newline (current-error-port)))

(add-to-load-path ".")


;;; Enabling and disabling debugging features of the host Scheme.

(read-enable 'positions)
